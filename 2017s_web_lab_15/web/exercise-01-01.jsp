<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.util.Date" %>
<!DOCTYPE html>
<%@page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Time Displayer</title>
</head>

<body>

<section id="view" class="container">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum in orci ipsum. Praesent interdum, nisl
        consectetur auctor pulvinar, tellus ante fringilla massa, quis sodales urna purus ut eros. Proin tempor, velit non tempus consequat, magna eros ultricies diam, non tempor sapien sapien eget justo. Curabitur id venenatis felis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aenean condimentum, felis eu accumsan rutrum, neque odio sagittis velit, id bibendum lorem leo ac justo. Quisque ante justo, blandit eu imperdiet nec, viverra ut orci. Aenean efficitur consectetur magna, eu interdum justo tempus non. Nunc sit amet blandit sem, at rhoncus erat. Suspendisse a leo felis. Integer mi sapien, lobortis sit amet turpis ac, semper euismod tortor. Aliquam rhoncus dapibus lacus sit amet egestas.</p>
    <hr>


    <%
        LocalDateTime display = LocalDateTime.now();

    %>
    <p><%=display%></p>

</section>
</body>
</html>